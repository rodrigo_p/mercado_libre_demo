package dependencies

object SupportDependencies {
    val appcompat = "androidx.appcompat:appcompat:${Versions.appcompat}"
    val constraintlayout = "androidx.constraintlayout:constraintlayout:${Versions.constraintlayout}"
    val material_design = "com.google.android.material:material:${Versions.material_design}"
    val swipe_refresh_layout = "androidx.swiperefreshlayout:swiperefreshlayout:${Versions.swipe_refresh_layout}"
    val multidex = "androidx.multidex:multidex:${Versions.multidex_version}"
    val android_dagger_support = "com.google.dagger:dagger-android-support:${Versions.dagger}"
}