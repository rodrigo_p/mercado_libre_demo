package com.demo.mla.api.main

import androidx.lifecycle.LiveData
import com.demo.mla.api.main.responses.ProductSearchListResponse
import com.demo.mla.util.GenericApiResponse
import retrofit2.http.*

interface MLAApiMainService {


    @GET("MLA/search")
    fun searchListProducts(
        @Query("q") query: String
    ): LiveData<GenericApiResponse<ProductSearchListResponse>>


}









