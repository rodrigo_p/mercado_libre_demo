package com.demo.mla.api.main.responses

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class ProductSearchListResponse (
    @SerializedName("results")
    @Expose
    var results: List<ProductSearchResponse>
){
    override fun toString(): String {
        return "ProductSearchListResponse(results=$results)"
    }
}