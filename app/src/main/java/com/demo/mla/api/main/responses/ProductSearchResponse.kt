package com.demo.mla.api.main.responses

import androidx.room.ColumnInfo
import androidx.room.PrimaryKey
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class ProductSearchResponse (

    @SerializedName("id")
    @Expose
    var id: String,

    @SerializedName("title")
    @Expose
    var title: String,

    @SerializedName("price")
    @Expose
    var price: Double,

    @SerializedName("thumbnail")
    @Expose
    var thumbnail: String,

    @SerializedName("available_quantity")
    @Expose
    var available_quantity: Long,

    @SerializedName("condition")
    @Expose
    var condition: String
){
    override fun toString(): String {
        return "ProductSearchResponse(id=$id, " +
                "title='$title', " +
                "price='$price', " +
                "thumbnail='$thumbnail', " +
                "available_quantity=$available_quantity, " +
                "condition='$condition')"
    }
}