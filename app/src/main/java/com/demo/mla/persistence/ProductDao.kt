package com.demo.mla.persistence

import androidx.lifecycle.LiveData
import androidx.room.*
import com.demo.mla.models.Product

@Dao
interface ProductDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(product: Product): Long

    @Query("""SELECT * FROM product WHERE title LIKE '%' || :query || '%' """)
    fun getAllProducts(query: String):LiveData<List<Product>>
}






