package com.demo.mla.persistence

import androidx.room.Database
import androidx.room.RoomDatabase
import com.demo.mla.models.Product

@Database(entities = [Product::class], version = 4)
abstract class AppDatabase: RoomDatabase() {



    abstract fun getProductDao(): ProductDao

    companion object{
        val DATABASE_NAME: String = "app_db"
    }


}








