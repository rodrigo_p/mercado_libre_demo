package com.demo.mla.util

import androidx.lifecycle.LiveData

/**
 *  LiveData class con valor nulo
 */
class AbsentLiveData<T : Any?> private constructor(): LiveData<T>() {

    init {
        // usar post envez de set , ya q solo puede ser creado en cualquier hilo
        postValue(null)
    }

    companion object {
        fun <T> create(): LiveData<T> {
            return AbsentLiveData()
        }
    }
}