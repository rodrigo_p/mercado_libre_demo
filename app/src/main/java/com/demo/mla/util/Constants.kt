package com.demo.mla.util

class Constants {

    companion object{

        const val BASE_URL = "https://api.mercadolibre.com/sites/"
        const val NETWORK_TIMEOUT = 6000L
        const val TESTING_NETWORK_DELAY = 0L // fake network delay for testing
        const val TESTING_CACHE_DELAY = 0L // fake cache delay for testing

    }
}