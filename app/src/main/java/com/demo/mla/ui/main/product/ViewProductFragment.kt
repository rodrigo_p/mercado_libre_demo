package com.demo.mla.ui.main.product

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.demo.mla.R
import com.demo.mla.models.Product
import kotlinx.android.synthetic.main.product_view_fragment.*

class ViewProductFragment: BaseProductFragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        return inflater.inflate(R.layout.product_view_fragment, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setHasOptionsMenu(true)
        subscribeObservers()
    }

    fun  subscribeObservers(){
        viewModel.dataState.observe(viewLifecycleOwner, Observer { dataState ->
            stateChangeListener.onDataStateChange(dataState)
        })
        viewModel.viewState.observe(viewLifecycleOwner, Observer { viewState ->
            viewState.viewProductFields.product?.let {product->
                setPropertiesProduct(product)
            }
        })
    }
    fun setPropertiesProduct(product: Product){
        requestManager
            .load(product.thumbnail)
            .transition(DrawableTransitionOptions.withCrossFade())
            .into(product_image)

        product_price.text = "Precio: " + product.price.toString()
        product_condition.text = "Condición :" + product.condition
        prod_text2.text = product.title
    }
}