package com.demo.mla.ui.main.product.viewmodel

import android.net.Uri


fun ProductViewModel.getSearchQuery(): String {
    getCurrentViewStateOrNew().let {
        return it.productFields.searchQuery
    }
}











