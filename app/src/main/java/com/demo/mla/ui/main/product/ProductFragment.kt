package com.demo.mla.ui.main.product

import android.app.SearchManager
import android.content.Context
import android.os.Bundle
import android.view.*
import android.view.inputmethod.EditorInfo
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.demo.mla.R
import com.demo.mla.models.Product
import com.demo.mla.ui.main.product.state.ProductStateEvent
import com.demo.mla.util.TopSpacingItemDecoration
import kotlinx.android.synthetic.main.product_fragment.*

class ProductFragment: BaseProductFragment(),
    ProductListAdapter.Interaction

{
    private lateinit var recyclerAdapter: ProductListAdapter
    private lateinit var searchView: SearchView
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.product_fragment,container,false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (activity as AppCompatActivity).supportActionBar?.setDisplayShowTitleEnabled(false)
        setHasOptionsMenu(true)


        initRecyclerView()
        subscribeObservers()

    }

    private fun subscribeObservers(){
        viewModel.dataState.observe(viewLifecycleOwner, Observer { dataState ->
            if(dataState != null){
                stateChangeListener.onDataStateChange(dataState)
                dataState.data?.let {
                    it.data?.let { event ->
                        event.getContentIfNotHandled()?.let {
                            viewModel.setProductList(it.productFields.productList)
                        }
                    }
                }
            }
        })
        viewModel.viewState.observe(viewLifecycleOwner, Observer { viewState->
            if(viewState != null){
                recyclerAdapter.apply {
                    preloadGlideImages(
                        requestManager = requestManager,
                        list = viewState.productFields.productList
                    )
                    submitList(
                        productList = viewState.productFields.productList
                    )
                }

            }
        })

    }
    private fun initRecyclerView(){
        product_recyclerview.apply {

            layoutManager = LinearLayoutManager(this@ProductFragment.context)
            val topSpacingDecorator = TopSpacingItemDecoration(30)
            removeItemDecoration(topSpacingDecorator) // does nothing if not applied already
            addItemDecoration(topSpacingDecorator)

            recyclerAdapter = ProductListAdapter(
                requestManager = requestManager,
                interaction = this@ProductFragment
            )
            adapter = recyclerAdapter
        }
    }
    private fun initSearchView(menu: Menu){
        activity?.apply {
            val searchManager: SearchManager = getSystemService(Context.SEARCH_SERVICE) as SearchManager
            searchView = menu.findItem(R.id.action_search).actionView as SearchView
            searchView.setSearchableInfo(searchManager.getSearchableInfo(componentName))
            searchView.maxWidth = Integer.MAX_VALUE
            searchView.setIconifiedByDefault(true)
            searchView.isSubmitButtonEnabled = true
        }

        val search = searchView.findViewById(R.id.search_src_text) as EditText
        search.setOnEditorActionListener { v, actionId, event ->

            if (actionId == EditorInfo.IME_ACTION_UNSPECIFIED
                || actionId == EditorInfo.IME_ACTION_SEARCH ) {
                val searchQuery = v.text.toString()

                viewModel.setQuery(searchQuery).let{
                    viewModel.setStateEvent(ProductStateEvent.ProductSearchEvent())
                }
            }
            true
        }
        val searchButton = searchView.findViewById(R.id.search_go_btn) as View
        searchButton.setOnClickListener {
            val searchQuery = search.text.toString()
            viewModel.setQuery(searchQuery).let {
                viewModel.setStateEvent(ProductStateEvent.ProductSearchEvent())
            }

        }

    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.search_menu, menu)
        initSearchView(menu)
    }
    override fun onDestroyView() {
        super.onDestroyView()
        product_recyclerview.adapter = null
    }
    override fun onItemSelected(position: Int, item: Product) {
        viewModel.setProduct(item)
         findNavController().navigate(R.id.action_productFragment_to_viewProductFragment)

    }

}