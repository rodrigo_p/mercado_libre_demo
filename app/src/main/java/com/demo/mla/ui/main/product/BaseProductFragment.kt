package com.demo.mla.ui.main.product

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.NavigationUI
import com.bumptech.glide.RequestManager
import com.demo.mla.R
import com.demo.mla.ui.main.DataStateChangeListener
import com.demo.mla.ui.main.product.viewmodel.ProductViewModel
import com.demo.mla.viewmodels.ViewModelProviderFactory
import dagger.android.support.DaggerFragment
import java.lang.Exception
import javax.inject.Inject

abstract  class BaseProductFragment: DaggerFragment() {
    val TAG: String = "AppDebug"

    @Inject
    lateinit var providerFactory : ViewModelProviderFactory
    lateinit var viewModel: ProductViewModel
    lateinit var stateChangeListener: DataStateChangeListener
    @Inject
    lateinit var requestManager: RequestManager

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupActionBarWithNavController(R.id.productFragment, activity as AppCompatActivity)

        viewModel = activity?.run {
            ViewModelProvider(this,providerFactory).get(ProductViewModel::class.java)
        }?:throw Exception("Invalid activity")

    }
    fun setupActionBarWithNavController(fragmentId: Int, activity: AppCompatActivity){
        val appBarConfiguration = AppBarConfiguration(setOf(fragmentId))
        NavigationUI.setupActionBarWithNavController(
            activity,
            findNavController(),
            appBarConfiguration
        )
    }
     fun cancelingJobs(){
        viewModel.cancelActiveJobs()
    }
    override fun onAttach(context: Context) {
        super.onAttach(context)
        try{
            stateChangeListener = context as DataStateChangeListener
        }catch(e: ClassCastException){
            Log.e(TAG, "$context must implement DataStateChangeListener" )
        }
    }
}