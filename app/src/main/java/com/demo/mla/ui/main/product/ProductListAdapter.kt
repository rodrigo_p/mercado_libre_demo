package com.demo.mla.ui.main.product

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.*
import com.bumptech.glide.RequestManager
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade
import com.demo.mla.R
import com.demo.mla.models.Product
import com.demo.mla.util.GenericViewHolder
import kotlinx.android.synthetic.main.layout_product_list_item.view.*
import kotlinx.android.synthetic.main.layout_product_list_item.view.product_image



class ProductListAdapter(
    private val requestManager: RequestManager,
    private val interaction: Interaction? = null
    ) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val TAG: String = "AppDebug"
    private val NO_MORE_RESULTS = -1
    private val PRODUCT_ITEM = 0
    private val NO_MORE_RESULTS_PRODUCTLIST = Product(
         "",
        "" ,
        0.0,
        "",
        0,
        ""

    )

    val DIFF_CALLBACK = object : DiffUtil.ItemCallback<Product>() {

        override fun areItemsTheSame(oldItem: Product, newItem: Product): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: Product, newItem: Product): Boolean {
            return oldItem == newItem
        }

    }
    private val differ =
        AsyncListDiffer(
            ProductRecyclerChangeCallback(this),
            AsyncDifferConfig.Builder(DIFF_CALLBACK).build()
        )


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {

        when(viewType){

            NO_MORE_RESULTS -> {
                Log.e(TAG, "onCreateViewHolder: Sin resultados...")
                return GenericViewHolder(
                    LayoutInflater.from(parent.context).inflate(
                        R.layout.layout_no_results,
                        parent,
                        false
                    )
                )
            }

            PRODUCT_ITEM ->{
                return ProductViewHolder(
                    LayoutInflater.from(parent.context).inflate(
                            R.layout.layout_product_list_item,
                        parent,
                        false
                    ),
                    interaction = interaction,
                    requestManager = requestManager
                )
            }
            else -> {
                return ProductViewHolder(
                    LayoutInflater.from(parent.context).inflate(
                        R.layout.layout_product_list_item,
                        parent,
                        false
                    ),
                    interaction = interaction,
                    requestManager = requestManager
                )
            }
        }
    }

    internal inner class ProductRecyclerChangeCallback(
        private val adapter: ProductListAdapter
    ) : ListUpdateCallback {

        override fun onChanged(position: Int, count: Int, payload: Any?) {
            adapter.notifyItemRangeChanged(position, count, payload)
        }

        override fun onInserted(position: Int, count: Int) {
            adapter.notifyItemRangeChanged(position, count)
        }

        override fun onMoved(fromPosition: Int, toPosition: Int) {
            adapter.notifyDataSetChanged()
        }

        override fun onRemoved(position: Int, count: Int) {
            adapter.notifyDataSetChanged()
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is ProductViewHolder -> {
                holder.bind(differ.currentList.get(position))
            }
        }
    }

    override fun getItemViewType(position: Int): Int {
        if(differ.currentList.get(position).id.length > -1){
            return 0
        }
        return -1
    }

    override fun getItemCount(): Int {
        return differ.currentList.size
    }


    fun preloadGlideImages(
        requestManager: RequestManager,
        list: List<Product>
    ){
        for(product in list){
            requestManager
                .load(product.thumbnail)
                .preload()
        }
    }

    fun submitList(productList: List<Product>?){
        val newList = productList?.toMutableList()
        differ.submitList(newList)
    }

    class ProductViewHolder
    constructor(
        itemView: View,
        val requestManager: RequestManager,
        private val interaction: Interaction?
    ) : RecyclerView.ViewHolder(itemView) {

        fun bind(item: Product) = with(itemView) {
            itemView.setOnClickListener {
                interaction?.onItemSelected(adapterPosition, item)
            }

            requestManager
                .load(item.thumbnail)
                .transition(withCrossFade())
                .into(itemView.product_image)

            itemView.product_price.text = "Precio :" + item.price.toString()
            itemView.product_.text = "Estado: " + item.condition
        }
    }

    interface Interaction {
        fun onItemSelected(position: Int, item: Product)
    }
}
