package com.demo.mla.ui.main.product.state

import com.demo.mla.models.Product

data class ProductViewState (
        var productFields: ProductFields = ProductFields(),
        var viewProductFields: ViewProductFields = ViewProductFields()
){
        data class ProductFields(
            var productList  : List<Product> = ArrayList(),
            var searchQuery: String = ""
        )
        data class ViewProductFields(
            var product: Product? = null
        )
}