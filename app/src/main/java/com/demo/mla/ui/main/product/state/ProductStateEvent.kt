package com.demo.mla.ui.main.product.state

sealed class ProductStateEvent {

        class ProductSearchEvent: ProductStateEvent()

        class None : ProductStateEvent()
}