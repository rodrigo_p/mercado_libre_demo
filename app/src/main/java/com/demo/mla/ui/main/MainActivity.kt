package com.demo.mla.ui.main

import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.fragment.app.Fragment
import androidx.navigation.NavController
import com.demo.mla.R
import com.demo.mla.ui.BaseActivity
import com.demo.mla.ui.DataState
import com.demo.mla.ui.main.product.BaseProductFragment
import com.demo.mla.ui.main.product.ViewProductFragment
import com.demo.mla.ui.main.product.state.ProductViewState
import com.demo.mla.util.BottomNavController
import com.demo.mla.util.setUpNavigation
import com.google.android.material.bottomnavigation.BottomNavigationView
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : BaseActivity(),
    BottomNavController.NavGraphProvider,
    BottomNavController.OnNavigationGraphChanged,
    BottomNavController.OnNavigationReselectedListener
{

    private lateinit var bottomNavigationView: BottomNavigationView
    private val bottomNavController by lazy(LazyThreadSafetyMode.NONE) {
        BottomNavController(
            this,
            R.id.main_nav_host_fragment,
            R.id.nav_product,
            this,
            this)
    }

    override fun getNavGraphId(itemId: Int)= when(itemId){
        R.id.nav_product -> {
            R.navigation.nav_product
        }

        else -> {
            R.navigation.nav_product
        }
    }

    override fun onGraphChange() {
        cancelActiveJobs()
    }
    private fun cancelActiveJobs(){
        val fragments = bottomNavController.fragmentManager
            .findFragmentById(bottomNavController.containerId)
            ?.childFragmentManager
            ?.fragments
        if(fragments != null){
            for(fragment in fragments){
                if(fragment is BaseProductFragment){
                    fragment.cancelingJobs()
                }

            }
        }
        displayProgressBar(false)
    }
    override fun onReselectNavItem(navController: NavController,
                                   fragment: Fragment) = when(fragment){

        is ViewProductFragment -> {
            navController.navigate(R.id.action_viewProductFragment_to_productFragment)
        }

        else -> {

        }
    }

    override fun onBackPressed()  = bottomNavController.onBackPressed()
    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        when(item?.itemId){
            android.R.id.home -> onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setupActionBar()
        bottomNavigationView = findViewById(R.id.bottom_navigation_view)
        bottomNavigationView.setUpNavigation(bottomNavController, this)
        if (savedInstanceState == null) {
            bottomNavController.onNavigationItemSelected()
        }
    }

    override fun displayProgressBar(bool: Boolean){
        if(bool){
            progress_bar.visibility = View.VISIBLE
        }
        else{
            progress_bar.visibility = View.GONE
        }
    }
    private fun setupActionBar(){
        setSupportActionBar(tool_bar)
    }
}