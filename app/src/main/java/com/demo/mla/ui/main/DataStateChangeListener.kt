package com.demo.mla.ui.main

import com.demo.mla.ui.DataState
import com.demo.mla.ui.main.product.state.ProductViewState

interface DataStateChangeListener{

    fun onDataStateChange(dataState: DataState<ProductViewState>)
}