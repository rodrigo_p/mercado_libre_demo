package com.demo.mla.ui.main.product.viewmodel

import androidx.lifecycle.LiveData
import com.demo.mla.models.Product
import com.demo.mla.repository.main.ProductRepository
import com.demo.mla.ui.BaseViewModel
import com.demo.mla.ui.DataState
import com.demo.mla.ui.main.product.state.ProductStateEvent
import com.demo.mla.ui.main.product.state.ProductViewState
import com.demo.mla.util.AbsentLiveData
import javax.inject.Inject

class ProductViewModel
@Inject
constructor(
    private val productRepository: ProductRepository
): BaseViewModel<ProductStateEvent,ProductViewState>(){
    override fun handleStateEvent(stateEvent: ProductStateEvent): LiveData<DataState<ProductViewState>> {
        when (stateEvent){
            is ProductStateEvent.ProductSearchEvent -> {
                return productRepository.searchProducts(getSearchQuery())?:AbsentLiveData.create()
            }
            is ProductStateEvent.None -> {
                return AbsentLiveData.create()
            }
        }
    }

    override fun initNewViewState(): ProductViewState {
            return ProductViewState()
    }

    fun setQuery(query: String){
        val update  = getCurrentViewStateOrNew()
        update.productFields.searchQuery = query
        _viewState.value = update
    }
    fun setProduct(product: Product){
        val update = getCurrentViewStateOrNew()
        update.viewProductFields.product = product
        _viewState.value = update
    }
    fun setProductList(productlist: List<Product>){
        val update  = getCurrentViewStateOrNew()
        update.productFields.productList = productlist
        _viewState.value = update
    }
     fun cancelActiveJobs(){
        productRepository.cancelActiveJobs()
        handlePendingData()
    }
    private fun handlePendingData(){
        setStateEvent(ProductStateEvent.None())
    }

    override fun onCleared() {
        super.onCleared()
        cancelActiveJobs()
    }
}