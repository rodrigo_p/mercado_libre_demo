package com.demo.mla.di.main


import com.demo.mla.ui.main.product.ProductFragment
import com.demo.mla.ui.main.product.ViewProductFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class MainFragmentBuildersModule {

    @ContributesAndroidInjector()
    abstract fun contributeProductFragment(): ProductFragment

    @ContributesAndroidInjector()
    abstract fun contributeViewProductFragment(): ViewProductFragment

}