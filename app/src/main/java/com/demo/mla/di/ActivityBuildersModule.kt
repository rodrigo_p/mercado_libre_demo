package com.demo.mla.di

import com.demo.mla.di.main.MainFragmentBuildersModule
import com.demo.mla.di.main.MainModule
import com.demo.mla.di.main.MainScope
import com.demo.mla.di.main.MainViewModelModule
import com.demo.mla.ui.main.MainActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector
@Module
abstract class ActivityBuildersModule {

    @MainScope
    @ContributesAndroidInjector(
        modules = [MainModule::class, MainFragmentBuildersModule::class, MainViewModelModule::class]
    )
    abstract fun contributeMainActivity() : MainActivity
}