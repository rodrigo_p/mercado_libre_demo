package com.demo.mla.di.main

import com.demo.mla.api.main.MLAApiMainService
import com.demo.mla.persistence.AppDatabase
import com.demo.mla.persistence.ProductDao
import com.demo.mla.repository.main.ProductRepository
import com.demo.mla.session.SessionManager
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit

@Module
class MainModule {

    @MainScope
    @Provides
    fun provideOpenApiMainService(retrofitBuilder: Retrofit.Builder): MLAApiMainService {
        return retrofitBuilder
            .build()
            .create(MLAApiMainService::class.java)
    }

    @MainScope
    @Provides
    fun provideProductDao(db: AppDatabase): ProductDao {
        return db.getProductDao()
    }

    @MainScope
    @Provides
    fun provideProductRepository(
        mlaApiMainService: MLAApiMainService,
        productDao: ProductDao,
        sessionManager: SessionManager
    ):ProductRepository{
            return ProductRepository(mlaApiMainService,productDao,sessionManager)
    }

}