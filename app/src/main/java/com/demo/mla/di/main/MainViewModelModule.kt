package com.demo.mla.di.main

import androidx.lifecycle.ViewModel
import com.demo.mla.di.ViewModelKey
import com.demo.mla.ui.main.product.viewmodel.ProductViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class MainViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(ProductViewModel::class)
    abstract fun bindProductViewModel(productViewModel: ProductViewModel): ViewModel
}