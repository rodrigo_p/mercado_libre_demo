package com.demo.mla.repository.main


import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.switchMap
import com.demo.mla.api.main.MLAApiMainService
import com.demo.mla.api.main.responses.ProductSearchListResponse
import com.demo.mla.models.Product
import com.demo.mla.persistence.ProductDao
import com.demo.mla.repository.JobManager
import com.demo.mla.repository.NetworkBoundResource
import com.demo.mla.session.SessionManager
import com.demo.mla.ui.DataState
import com.demo.mla.ui.main.product.state.ProductViewState
import com.demo.mla.util.ApiSuccessResponse
import com.demo.mla.util.GenericApiResponse
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.Dispatchers.Main
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.lang.Exception
import javax.inject.Inject

class ProductRepository
@Inject
constructor(
    val productMLAApiMainService: MLAApiMainService,
    val productDao: ProductDao,
    val sessionManager: SessionManager
): JobManager(){
    private val TAG: String = "AppDebug"

        fun searchProducts(
            query: String
        ):LiveData<DataState<ProductViewState>>{
            return object : NetworkBoundResource<ProductSearchListResponse,List<Product>,ProductViewState>(
                sessionManager.isConnectedToTheInternet(),
                true,
                false,
                true
            ){
                override suspend fun createCacheRequestAndReturn() {
                    withContext(Main){
                        result.addSource(loadFromCache()){viewState ->
                            onCompleteJob(DataState.data(viewState,null))
                        }
                    }
                }

                override suspend fun handleApiSuccessResponse(response: ApiSuccessResponse<ProductSearchListResponse>) {
                            val productList : ArrayList<Product> = ArrayList()
                            for (prListResponse in response.body.results){
                                productList.add(
                                    Product(
                                        id = prListResponse.id,
                                        title = prListResponse.title,
                                        thumbnail = prListResponse.thumbnail,
                                        price = prListResponse.price,
                                        available_quantity = prListResponse.available_quantity,
                                        condition = prListResponse.condition
                                    )
                                )
                            }
                    updateLocalDb(productList)
                    createCacheRequestAndReturn()
                }

                override fun createCall(): LiveData<GenericApiResponse<ProductSearchListResponse>> {
                    return productMLAApiMainService.searchListProducts(
                        query = query
                    )
                }

                override fun loadFromCache(): LiveData<ProductViewState> {
                        return productDao.getAllProducts(query)
                            .switchMap {
                                object : LiveData<ProductViewState>(){
                                    override fun onActive() {
                                        super.onActive()
                                        value = ProductViewState(
                                            ProductViewState.ProductFields(
                                                productList = it
                                            )
                                        )
                                    }
                                }
                            }
                }

                override suspend fun updateLocalDb(cacheObject: List<Product>?) {
                        if(cacheObject != null){
                            withContext(IO){
                                for(pr in cacheObject){
                                    try {
                                        launch {
                                            Log.e(TAG, "insert producto:$pr "  )
                                            productDao.insert(pr)
                                        }
                                    }catch (e: Exception){
                                        Log.e(TAG,"Bd: error actualizando producto")
                                    }
                                }
                            }
                        }
                }

                override fun setJob(job: Job) {
                        addJob("search productos",job)
                }
            }.asLiveData()
        }
}