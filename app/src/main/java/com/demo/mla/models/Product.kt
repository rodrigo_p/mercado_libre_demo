package com.demo.mla.models

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Product (

    @PrimaryKey(autoGenerate = false)
    @ColumnInfo(name = "id")
    var id: String,

    @ColumnInfo(name = "title")
    var title: String,

    @ColumnInfo(name = "price")
    var price: Double,

    @ColumnInfo(name = "thumbnail")
    var thumbnail: String,

    @ColumnInfo(name = "available_quantity")
    var available_quantity: Long,

    @ColumnInfo(name = "condition")
    var condition: String
){
    override fun toString(): String {
        return "Product(id=$id, " +
                "title='$title', " +
                "price='$price', " +
                "thumbnail='$thumbnail', " +
                "available_quantity=$available_quantity, " +
                "condition='$condition')"
    }
}